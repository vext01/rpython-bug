import sys

def target(*args):
    from stuff import entry_point
    return entry_point, None

def jitpolicy(driver):
    from rpython.jit.codewriter.policy import JitPolicy
    return JitPolicy()

if __name__ == "__main__":
    from stuff import entry_point
    entry_point(sys.argv)
