from pypy.interpreter.gateway import interp2app, unwrap_spec
from pypy.interpreter.baseobjspace import W_Root

class W_MyBase(W_Root):
    def do_something(self, arg1, arg2, unused=None):
        print("In base: %s %s" % (arg1, arg2))

class W_MySuper(W_MyBase):
    def do_something(self, arg_one, arg2, unused=None):
        print("In super: %s %s" % (arg_one, arg2))
        W_MyBase.do_something(self, arg_one, arg2)

def entry_point(args):
    if int(args[1]) < 0:
        x = W_MyBase()
    else:
        x = W_MySuper()

    x.do_something(666, 667)

    return 0
